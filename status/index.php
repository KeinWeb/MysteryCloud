<?php
/**
 * +-----------------------------------------------------------------------+
 * | This file is part of the MysteryCloud website                         |
 * |                                                                       |
 * | Copyright (C) KeinWeb Inc.                                            |
 * |                                                                       |
 * | Licensed under the MIT Massachusetts Institute of Technology          |
 * | for open source http://www.opensource.org/licenses/mit-license.php    |
 * | See the LICENSE file for a full license statement.                    |
 * |                                                                       |
 * | PURPOSE:                                                              |
 * |   Provide the landingpage of Mysterycloud.eu                          |
 * +-----------------------------------------------------------------------+
 * | Author: Yannick Hillemacher <hillemacher@keinweb.com>                 |
 * | Website: https://keinweb.com                                          |
 * +-----------------------------------------------------------------------+
 */


function getData(string $name)
{

    switch ($name) {

        case "server":
            return json_decode(file_get_contents(__DIR__ . "/../cronjob/serverdata.myc"), true);
    }
    return '';
}


?>
<!doctype html>
<html lang="de-DE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="author" content="MysteryCloud developer yannick"/>
    <meta name="description" content="Dies is die offizielle Website von MysteryCloud"/>
    <meta name="keywords" content="Mystery, Cloud, MysteryCloud, Minecraft, Forum, Mini games, Server"/>
    <meta name="copyright" content="rights reserved by MysteryCloud.eu"/>
    <title>MysteryCloud | Status</title>
    <link rel="stylesheet" href="../vendor/fontawesome/brands.min.css">
    <link rel="stylesheet" href="../vendor/build/bootstrap.min.css">
    <link rel="stylesheet" href="../vendor/fontawesome/all.css">
    <link rel="stylesheet" href="../vendor/fontawesome/light.min.css">
    <link rel="stylesheet" href="../vendor/fontawesome/solid.min.css">
    <link rel="stylesheet" href="../vendor/fontawesome/regular.min.css">
    <link rel="stylesheet" href="../vendor/fontawesome/duotone.min.css">
    <link rel="stylesheet" href="../vendor/build/myc_main.css">
</head>
<body>
<button onclick="topFunction()" id="myc_scroll_top"><i class="far fa-arrow-up"></i></button>
<section class="header myc_no_select" id="header" style="height: 70vh">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12" id="myc_mobile_background">
                <div class="myc_topbar d-flex justify-content-evenly">
                    <div class="myc_logo">
                        <img src="../vendor/img/64x64.png" class="myc_no_select" alt="MysteryCloud Logo" width="50"
                             height="50">
                        <a href="../" class="myc_no_select">
                            <myc_highlight>Mystery</myc_highlight>
                            Cloud</a>
                    </div>
                    <div class="myc_items">
                        <a href="#" class="myc_no_select"><i class="fas fa-comments"></i>Forum</a>
                        <a href="#" class="myc_no_select"><i class="fas fa-users"></i>Team</a>
                        <a href=" " class="myc_active"><i class="fas fa-signal-alt"></i>Status</a>
                        <a href="../#news" class="myc_no_select"><i class="fas fa-envelope"></i>News</a>
                    </div>
                    <div class="myc_copy_ip d-flex justify-content-center">
                        <a onclick="copyIP()" href="#" class="myc_no_select"><i class="fas fa-clipboard-list"></i><span>Copy!</span>
                            <br><span>MysteryCloud.eu</span></a>
                    </div>
                    <!-- Mobile Topbar Toggler -->
                    <div class="myc_mobile">
                        <a onclick="mobileCollapse()" id="myc_mobile_items_toggler" class="navbar-toggler" type="button"
                           data-bs-toggle="collapse" data-bs-target="#mobileNav" aria-controls="mobileNav"
                           aria-expanded="false" aria-label="Toggle navigation"><i id="myc_mobile_icon"
                                                                                   class="far fa-bars"></i></a>
                    </div>
                </div>
                <!-- Mobile Topbar Item List -->
                <div class="myc_mobile_items collapse navbar-collapse" id="mobileNav">
                    <a href="#"><i class="fas fa-comments"></i>Forum</a>
                    <a href="#"><i class="fas fa-users"></i>Team</a>
                    <a href="#" class="myc_active"><i class="fas fa-signal-alt"></i>Status</a>
                    <a href="../#news"><i class="fas fa-envelope"></i>News</a>
                </div>
            </div>
            <div class="col-12">
                <div class="myc_header_content">
                    <div class="myc_rights_title text-center">
                        <p>Status</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="status">
    <div class="container">
        <div class="row">
            <div class="col-12 myc_status_element">
                <div class="myc_name">
                    <p>Netzwerk</p>
                </div>
                <div class="myc_content">
                    <div class="row">

                    <?php

                    $server = getData("server");
                    echo '<div class="col-12 col-md-6 col-lg-5 col-xl-4 col-xxl-3">
                        <p>
                        <br>
                        <myc_highlight><span style="margin-right: 1rem">MOTD:</span></myc_highlight> '.$server['motd'].'
                        <br>
                        <br>
                        <myc_highlight><span style="margin-right: 1rem">Version:</span></myc_highlight> '.$server['version'].'
                        <br>
                        <br>
                    </p></div>';
                    echo '<div class="col-12 col-md-6 col-lg-5 col-xl-4 col-xxl-4">
                        <p>
                        <br>
                        <myc_highlight><span style="margin-right: 1rem">Spieler:</span></myc_highlight> '.$server['players'].'/'.$server['max_players'].'
                        <br>
                        <br>
                        <myc_highlight><span style="margin-right: 1rem">Letzter Stand:</span></myc_highlight> '.$server['time'].' Uhr - '.$server['date'].'
                        <br>
                        <br>
                    </p></div>';
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <div class="myc_impressum">
                    <a href="../impressum/">Impressum</a>
                </div>
                <div class="myc_datenschutz">
                    <a href="../datenschutz">Datenschutz</a>
                </div>
            </div>
            <div class="col-12 d-flex justify-content-center">
                <div class="myc_copyright">
                    <p>&copy; 2022
                        <myc_highlight>Mystery</myc_highlight>Cloud.eu
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="../vendor/js/jquery.min.js"></script>
<script src="../vendor/js/myc_app.js"></script>
<script src="../vendor/js/bootstrap.bundle.min.js"></script>
</body>
</html>