/**
 +-----------------------------------------------------------------------+
 | This file is part of the MysteryCloud website                         |
 |                                                                       |
 | Copyright (C) KeinWeb Inc.                                            |
 |                                                                       |
 | Licensed under the MIT Massachusetts Institute of Technology          |
 | for open source http://www.opensource.org/licenses/mit-license.php    |
 | See the LICENSE file for a full license statement.                    |
 |                                                                       |
 | PURPOSE:                                                              |
 |   Provide the landingpage of Mysterycloud.eu                          |
 +-----------------------------------------------------------------------+
 | Author: Yannick Hillemacher <hillemacher@keinweb.com>                 |
 | Website: https://keinweb.com                                          |
 +-----------------------------------------------------------------------+
 */


// Stored elements for topbar usage
var itemsList = document.getElementById('mobileNav');
var menuBg = document.getElementById('myc_mobile_background');
var menuIcon = document.getElementById('myc_mobile_icon');


function copyIP() {
    navigator.clipboard.writeText("mystercloud.eu");
}

// Some fixed design actions for topbar collapsing
function mobileCollapse() {
    var test = document.getElementById('myc_mobile_items_toggler');
    if (test.classList.contains("collapsed") === false) {
        menuIcon.classList.replace('fa-bars', 'fa-times');
        menuBg.style.backgroundColor = 'rgba(51, 51, 51, .20)';
    } else {
        menuIcon.classList.replace('fa-times', 'fa-bars');
        menuBg.style.backgroundColor = 'transparent';
    }
}

// Listener for resize to clear mobile topbar appearance
window.addEventListener('resize', function () {
    itemsList.classList.remove('show')
    menuIcon.classList.replace('fa-times', 'fa-bars');
    menuBg.style.backgroundColor = 'transparent';
})



// Scroll to top function
var scrollButton = document.getElementById("myc_scroll_top");

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        scrollButton.style.display = "block";
    } else {
        scrollButton.style.display = "none";
    }
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
