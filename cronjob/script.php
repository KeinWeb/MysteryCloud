<?php

if ($_SERVER['PHP_AUTH_USER'] == hash("sha256","mysterycloud.eu/ifB^A1kiV6fd")) {
    if ($_SERVER['PHP_AUTH_PW'] == hash("sha256","aH^F64AhE1ibMaIJmK2vlYWDwz8qg^gXxzsh^c%voqVgCZE%82")) {


        class ServerStatusQuery {

            public function getServerInfo( $address = "37.114.56.16", $port = 25565, $timeout = 5 ) {

                if( !filter_var( $address, FILTER_VALIDATE_IP ) ) {
                    $address = gethostbyname( $address );
                }

                $socket = stream_socket_client("tcp://" . $address . ":" . $port, $errorNumber, $errorString, $timeout );

                if ( !$socket ) {

                    return false;

                } else {

                    stream_set_timeout( $socket, $timeout );

                    fwrite ( $socket, "\XFE\01" );

                    $data = fread( $socket, 2048 );

                    fclose( $socket );

                    if( $data == null ) {
                        return false;
                    }

                    $content = explode( "\x00", mb_convert_encoding(substr((String)$data, 15), 'UTF-8', 'UCS-2' ) );
                    $motd = preg_replace("/(§.)/", "",$content[1]);
                    $version = preg_replace("/(§.)/", "",$content[2]);
                    $motd = preg_replace("/[^[:alnum:][:punct:] ]/", "", $motd);
                    $version = preg_replace("/[^[:alnum:][:punct:] ]/", "", $version);

                    return json_encode(array(
                        "motd"			=>	$motd,
                        "version"			=>	$version,
                        "players"		=>	$content[ 3 ],
                        "max_players"	=>	$content[ 4 ],
                        "date"	=>	date("d.m.Y"),
                        "time"	=>	date("H:i")
                    ));
                }
            }
        };

        $status = new ServerStatusQuery();
        file_put_contents(__DIR__ . '/serverdata.myc', $status->getServerInfo("37.114.56.16", 25565));


    } else {
        header('WWW-Authenticate: Basic realm="Failed"');
        header('HTTP/1.0 401 Unauthorized');
    }
} else {
    header('WWW-Authenticate: Basic realm="Failed"');
    header('HTTP/1.0 401 Unauthorized');
}
