<?php
/**
+-----------------------------------------------------------------------+
| This file is part of the MysteryCloud website                         |
|                                                                       |
| Copyright (C) KeinWeb Inc.                                            |
|                                                                       |
| Licensed under the MIT Massachusetts Institute of Technology          |
| for open source http://www.opensource.org/licenses/mit-license.php    |
| See the LICENSE file for a full license statement.                    |
|                                                                       |
| PURPOSE:                                                              |
|   Provide the landingpage of Mysterycloud.eu                          |
+-----------------------------------------------------------------------+
| Author: Yannick Hillemacher <hillemacher@keinweb.com>                 |
| Website: https://keinweb.com                                          |
+-----------------------------------------------------------------------+
 */


?>
<!doctype html>
<html lang="de-DE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="author" content="MysteryCloud developer yannick" />
    <meta name="description" content="Dies is die offizielle Website von MysteryCloud" />
    <meta name="keywords" content="Mystery, Cloud, MysteryCloud, Minecraft, Forum, Mini games, Server" />
    <meta name="copyright" content="rights reserved by MysteryCloud.eu" />
    <title>MysteryCloud | Home</title>
    <link rel="stylesheet" href="vendor/fontawesome/brands.min.css">
    <link rel="stylesheet" href="vendor/build/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/fontawesome/all.css">
    <link rel="stylesheet" href="vendor/fontawesome/light.min.css">
    <link rel="stylesheet" href="vendor/fontawesome/solid.min.css">
    <link rel="stylesheet" href="vendor/fontawesome/regular.min.css">
    <link rel="stylesheet" href="vendor/fontawesome/duotone.min.css">
    <link rel="stylesheet" href="vendor/build/myc_main.css">
</head>
<body>
<button onclick="topFunction()" id="myc_scroll_top"><i class="far fa-arrow-up"></i></button>
<section class="header myc_no_select" id="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12" id="myc_mobile_background">
                <div class="myc_topbar d-flex justify-content-evenly">
                    <div class="myc_logo">
                        <img src="vendor/img/64x64.png" class="myc_no_select" alt="MysteryCloud Logo" width="50" height="50">
                        <a href=" " class="myc_no_select"><myc_highlight>Mystery</myc_highlight>Cloud</a>
                    </div>
                    <div class="myc_items">
                        <a href="#" class="myc_no_select"><i class="fas fa-comments"></i>Forum</a>
                        <a href="#" class="myc_no_select"><i class="fas fa-users"></i>Team</a>
                        <a href="status/"><i class="fas fa-signal-alt"></i>Status</a>
                        <a href="#news" class="myc_no_select"><i class="fas fa-envelope"></i>News</a>
                    </div>
                    <div class="myc_copy_ip d-flex justify-content-center">
                        <a onclick="copyIP()" href="#" class="myc_no_select"><i class="fas fa-clipboard-list"></i><span>Copy!</span> <br><span>MysteryCloud.eu</span></a>
                    </div>
                    <!-- Mobile Topbar Toggler -->
                    <div class="myc_mobile">
                        <a onclick="mobileCollapse()" id="myc_mobile_items_toggler" class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mobileNav" aria-controls="mobileNav" aria-expanded="false" aria-label="Toggle navigation"><i id="myc_mobile_icon" class="far fa-bars"></i></a>
                    </div>
                </div>
                <!-- Mobile Topbar Item List -->
                <div class="myc_mobile_items collapse navbar-collapse" id="mobileNav">
                    <a href="#"><i class="fas fa-comments"></i>Forum</a>
                    <a href="#"><i class="fas fa-users"></i>Team</a>
                    <a href="status/"><i class="fas fa-signal-alt"></i>Status</a>
                    <a href="#"><i class="fas fa-envelope"></i>News</a>
                </div>
            </div>
            <div class="col-12">
                <div class="myc_header_content">
                    <div class="myc_subject">
                        <p class="myc_no_select">Willkommen</p>
                    </div>
                    <div class="myc_description">
                        <p class="myc_no_select">Dies ist ein Beispiel Text für spätere<br>
                            Informationen, welche hier stehen können.<br>
                             Dieser kann später noch verändert werden,<br>
                              ganz nach individuellen Bedarf.
                        </p>
                    </div>
                    <div class="myc_actions">
                        <a href="#" class="myc_no_select">Community<i class="fas fa-comments"></i></a>
                    </div>
                </div>
                <div class="myc_clouds">
                    <div class="top"></div>
                    <div class="top_right"></div>
                    <div class="right"></div>
                    <div class="bottom"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="news" id="news">
    <div class="container">
        <div class="row d-flex justify-content-evenly">
            <div class="col-12 col-xxl-6" id="myc_news_col" style="margin-top: 4rem">
                <div class="myc_subject myc_no_select">
                    <p>Nachrichten</p>
                    <div class="myc_underline"></div>
                </div>
                <div class="myc_description">
                    <p>Hier findet ihr alle <myc_highlight>aktuellen</myc_highlight> Nachrichten über
                    <br>
                    unser Netzwerk. Um immer auf den <myc_highlight>neusten</myc_highlight> Stand
                    <br>
                    zu ein, und um nichts mehr zu <myc_highlight>verpassen</myc_highlight>, kannst du
                    <br>
                    unseren <myc_highlight>Newsletter</myc_highlight> folgen. Dort wirst du über Events,
                    <br>
                    Updates, Neuigkeiten und Bugfixes <myc_highlight>informiert</myc_highlight>.
                    <br>
                    <br>
                    <myc_highlight>Klingt doch gut!</myc_highlight>
                    <br>
                    Also verliere keine Zeit und trage deine <myc_highlight>Email</myc_highlight> ein
                    <br>
                    und genieße alle <myc_highlight>Vorteile</myc_highlight>.
                    </p>
                </div>
                <div class="myc_newsletter">
                    <form action="#" method="post">
                        <div class="col-12 col-md-7">
                            <div class="input-group">
                                <input type="text" name="" class="form-control" placeholder="maxmusterman@gmail.com" aria-describedby="myc_newsletter">
                                <button class="btn btn-outline-secondary" type="button" id="myc_newsletter">Folgen <i class="fas fa-paper-plane"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-12 col-xxl-6" style="margin-top: 13rem">
                <div class="myc_news_list">
                    <div class="myc_item">
                        <div class="row">
                            <div class="col-12">
                                <div class="myc_title">
                                    <p class="myc_selection_reverse">Neues Silvester Event kommt bald!</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="myc_description">
                                    <p>Sei bei dem neuen Silvester Event dabei und joine jetzt auf unseren Server.</p>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="myc_author">
                                    <p><i class="fas fa-user"></i>MysteryCloud</p>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="myc_date">
                                    <p><i class="fas fa-clock"></i>31.12.2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="myc_item">
                        <div class="row">
                            <div class="col-12">
                                <div class="myc_title">
                                    <p class="myc_selection_reverse">Neues Silvester Event kommt bald!</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="myc_description">
                                    <p>Sei bei dem neuen Silvester Event dabei und joine jetzt auf unseren Server.</p>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="myc_author">
                                    <p><i class="fas fa-user"></i>MysteryCloud</p>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="myc_date">
                                    <p><i class="fas fa-clock"></i>31.12.2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="myc_item">
                        <div class="row">
                            <div class="col-12">
                                <div class="myc_title">
                                    <p class="myc_selection_reverse">Neues Silvester Event kommt bald!</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="myc_description">
                                    <p>Sei bei dem neuen Silvester Event dabei und joine jetzt auf unseren Server.</p>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="myc_author">
                                    <p><i class="fas fa-user"></i>MysteryCloud</p>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="myc_date">
                                    <p><i class="fas fa-clock"></i>31.12.2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about">
    <div class="container">
        <div class="row myc_about">
            <div class="col-12 d-flex justify-content-center" style="margin-top: 8rem">
                <div class="myc_subject myc_no_select">
                    <p>Über uns</p>
                    <div class="myc_underline"></div>
                </div>
            </div>
            <div class="col-12 d-flex justify-content-center">
                <div class="myc_description">
                    <p>Das ist ein <myc_highlight>Beispiel</myc_highlight> text für eventuelle <myc_highlight>zukünftige</myc_highlight> über uns texte, und infos.
                    <br>Hier kann über die Entwicklung des <myc_highlight>Netzwerkes</myc_highlight> ein text stehen.
                    <br>Hier kann über die <myc_highlight>Entwicklung</myc_highlight> des Netzwerkes ein text stehen, oder <myc_highlight>informationen</myc_highlight>.
                    <br>Hier kann <myc_highlight>über</myc_highlight> die Entwicklung des Netzwerkes ein <myc_highlight>text</myc_highlight> stehen, oder informationen über die Gründung.
                    <br>Hier kann über die Entwicklung des Netzwerkes ein text stehen, oder <myc_highlight>informationen</myc_highlight>.
                    <br>Hier kann über <myc_highlight>die Entwicklung</myc_highlight> des Netzwerkes ein text stehen.
                    <br>Euer Liebes <myc_highlight>Mystery</myc_highlight>Cloud Netzwerk Team
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <div class="myc_impressum">
                    <a href="impressum/">Impressum</a>
                </div>
                <div class="myc_datenschutz">
                    <a href="datenschutz/">Datenschutz</a>
                </div>
            </div>
            <div class="col-12 d-flex justify-content-center">
                <div class="myc_copyright">
                    <p>&copy; 2022 <myc_highlight>Mystery</myc_highlight>Cloud.eu</p>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="vendor/js/jquery.min.js"></script>
<script src="vendor/js/myc_app.js"></script>
<script src="vendor/js/bootstrap.bundle.min.js"></script>
</body>
</html>